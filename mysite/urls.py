from django.conf.urls import patterns, include, url
from django.core.urlresolvers import reverse_lazy
from django.views.generic import RedirectView

urlpatterns = patterns('',
    url(r'^$', RedirectView.as_view(url=reverse_lazy('blog:index'))),
    # url(r'^$', RedirectView.as_view(url='/~p3/wsgi/blog/')),
    url(r'^blog/', include('microblog.urls', namespace='blog'))
)
