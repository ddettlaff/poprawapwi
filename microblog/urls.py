from django.conf.urls import patterns, url
from microblog import views
from django.contrib.auth.views import login, logout
from django.core.urlresolvers import reverse_lazy

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^add/$', views.add, name='add'),
    url(r'^users/$', views.users, name='users'),
    url(r'^user/(?P<id>\d+)/$', views.user_info, name='user_info'),
    url(r'^login/$', login, {'template_name': 'microblog/login.html'}, name='login'),
    url(r'^logout/$', logout, {'next_page': reverse_lazy('blog:index')} , name='logout')
)