from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Entry(models.Model):
    title = models.CharField(max_length=100)
    text = models.CharField(max_length=300)
    #author = models.CharField(max_length=50)
    author = models.ForeignKey(User)
    pub_date = models.DateTimeField('date published')