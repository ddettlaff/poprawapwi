# -*- coding: utf-8 -*-

from django.test import TestCase
from django.contrib.auth.models import User
from microblog.models import Entry
from django.utils import timezone


class MicroblogTest(TestCase):

    def test_get_home(self):
        response = self.client.get('/blog/')
        self.assertEqual(response.status_code, 200)


    def test_empty_entries_table(self):
        response = self.client.get('/blog/')
        self.assertContains(response, "Pusto")
        self.assertQuerysetEqual(response.context['entries'], [])


    def test_empty_users_table(self):
        response = self.client.get('/blog/users/')
        self.assertContains(response, u"Nie ma użytkowników")
        self.assertQuerysetEqual(response.context['users'], [])


    def test_users_list(self):
        n = 5
        TestHelpers.create_users(n)
        response = self.client.get('/blog/users/')
        self.assertContains(response, "test1")
        self.assertEqual(len(response.context['users']), n)


    def test_login_logout(self):
        TestHelpers.create_users(1)
        response = self.client.get('/blog/')
        self.assertContains(response, u'Zaloguj się')
        self.client.login(username='test1', password='test1')
        response = self.client.get('/blog/')
        self.assertContains(response, u'Wyloguj się')
        self.client.logout()
        response = self.client.get('/blog/')
        self.assertContains(response, u'Zaloguj się')


    def test_add_entry_anon(self):
        post_data = {'title': 'Tytul testowy', 'text': 'Tekst testowy'}
        response = self.client.post('/blog/add/', post_data)
        self.assertRedirects(response, '/blog/login/?next=/blog/add/')
        self.assertEqual(Entry.objects.count(), 0)


    def test_add_entry_user(self):
        TestHelpers.create_users(1)
        self.client.login(username='test1', password='test1')
        post_data = {'title': 'Tytul testowy', 'text': 'Tekst testowy'}
        response = self.client.post('/blog/add/', post_data, follow=True)
        self.assertContains(response, 'Tytul testowy')
        self.assertEqual(Entry.objects.count(), 1)
        self.assertEqual(Entry.objects.get(pk=1).author.username, 'test1')


    def test_user_entries(self):
        TestHelpers.create_user_entry()
        response = self.client.get('/blog/user/1/')
        self.assertContains(response, 'Tytul testowy')
        self.assertEqual(len(response.context['entries']), 1)


    def test_error_message(self):
        TestHelpers.create_users(1)
        self.client.login(username='test1', password='test1')
        post_data = {'title': '', 'text': ''}
        response = self.client.post('/blog/add/', post_data, follow=True)
        self.assertContains(response, 'Brakuje danych')



class TestHelpers:
    @staticmethod
    def create_users(i=2):
        for i in range(1, i+1):
            name = "test" + str(i)
            User.objects.create_user(username=name, password=name, email=name+'@test.com')

    @staticmethod
    def create_user_entry():
        TestHelpers.create_users(1)
        Entry.objects.create(title='Tytul testowy', text='Tekst testowy', author=User.objects.get(pk=1), pub_date=timezone.now())
