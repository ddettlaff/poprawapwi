from django.shortcuts import render, redirect
from microblog.models import Entry
from django.utils import timezone
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404


def index(request):
    entries = Entry.objects.all().order_by('-pub_date')
    return render(request, 'microblog/index.html', {'entries': entries})

@login_required
def add(request):
    if request.method == 'POST':
        data = request.POST
        if data.get('title') and data.get('text'):
            entry = Entry(title=data['title'], text=data['text'], author=request.user, pub_date=timezone.now())
            entry.save()
            messages.success(request, 'Dodano wpis!')
        else:
            messages.warning(request, 'Brakuje danych')
    return redirect(reverse('blog:index'))

def users(request):
    users_list = User.objects.all().order_by('username')
    return render(request, 'microblog/users.html', {'users': users_list})

def user_info(request, id):
    user = get_object_or_404(User, pk=int(id))
    entries = user.entry_set.all().order_by("-pub_date")

    return render(request, 'microblog/user.html', {'user_info': user, 'entries': entries})
